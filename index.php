<?php
// On enregistre notre autoload.
function chargerClasse($classname)
{
  require $classname.'.php';
}

spl_autoload_register('chargerClasse');

session_start(); // On appelle session_start() APRÈS avoir enregistré l'autoload.

if (isset($_GET['deconnexion']))
{
  session_destroy();
  header('Location: .');
  exit();
}

$db = new PDO('mysql:host=localhost;dbname=combats', 'root', 'password');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); // On émet une alerte à chaque fois qu'une requête a échoué.

$manager = new PersonnagesManager($db);

if (isset($_SESSION['perso'])) // Si la session perso existe, on restaure l'objet.
{
  $perso = $_SESSION['perso'];
}

if (isset($_POST['creer']) && isset($_POST['nom'])) // Si on a voulu créer un personnage.
{
  $perso = new Personnage(['nom' => $_POST['nom']]); // On crée un nouveau personnage.
  
  if (!$perso->nomValide())
  {
    $message = 'Le nom choisi est invalide.';
    unset($perso);
  }
  elseif ($manager->exists($perso->nom()))
  {
    $message = 'Le nom du personnage est déjà pris.';
    unset($perso);
  }
  else
  {
    $manager->add($perso);
  }
}

elseif (isset($_POST['utiliser']) && isset($_POST['nom'])) // Si on a voulu utiliser un personnage.
{
  if ($manager->exists($_POST['nom'])) // Si celui-ci existe.
  {
    $perso = $manager->get($_POST['nom']);
  }
  else
  {
    $message = 'Ce personnage n\'existe pas !'; // S'il n'existe pas, on affichera ce message.
  }
}

elseif (isset($_GET['frapper'])) // Si on a cliqué sur un personnage pour le frapper.
{
  if (!isset($perso))
  {
    $message = 'Merci de créer un personnage ou de vous identifier.';
  }
  
  else
  {
    if (!$manager->exists((int) $_GET['frapper']))
    {
      $message = 'Le personnage que vous voulez frapper n\'existe pas !';
    }
    
    else
    {
      $persoAFrapper = $manager->get((int) $_GET['frapper']);
      
      $retour = $perso->frapper($persoAFrapper); // On stocke dans $retour les éventuelles erreurs ou messages que renvoie la méthode frapper.
      
      switch ($retour)
      {
        case Personnage::CEST_MOI :
          $message = 'Mais... pourquoi voulez-vous vous frapper ???';
          break;
        
        case Personnage::PERSONNAGE_FRAPPE :
          $message = 'Le personnage a bien été frappé !';
          
          $manager->update($perso);
          $manager->update($persoAFrapper);
          
          break;
        
        case Personnage::PERSONNAGE_TUE :
          $message = 'Vous avez tué ce personnage !';
          
          $manager->update($perso);
          $manager->delete($persoAFrapper);
          
          break;
      }
    }
  }
}
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Fantasy Battle</title>
  <!-- Icon -->
  <link rel="icon" type="image/png" href="assets/image/logo/logo.png" />
  <!-- CSS -->
  <!-- CSS Stylesheets -->
  <link rel="stylesheet" type="text/css" media="screen" href="assets/css/style.css" />
  <!-- CSS Bootstrap 4.2.1 -->
  <link rel="stylesheet" type="text/css" media="screen" href="assets/css/bootstrap.css" />
  <!-- JS -->
  <!-- jQuery 3.3.1 -->
  <script src="assets/js/jquery-3.3.1.js"></script>
  <!-- JS Bootstrap 4.2.1 -->
  <script src="assets/js/bootstrap.js"></script>
</head>

<body>
  <h1 class="text-center font_morpheus"><img width="3%" style="transform: rotate(0.5turn);" src="assets/image/logo/logo.png" />
    <u>Fantasy Battle</u> <img width="3%" style="transform: rotate(0.25turn);" src="assets/image/logo/logo.png" /></h1><br />
  <h3 class="text-center"><i>Réveille la puissance qui est en toi...</i></h3>
  <!-- <div class="d-flex">
    <a class="btn btn-success mx-auto mt-2 mb-2" href="#">Jouer</a>
  </div> -->
  <div class="text-center pt-3 pb-2">
    <p>Nombre de personnages créés :
      <?= $manager->count() ?>
    </p>
  </div>
  <?php
if (isset($message)) // On a un message à afficher ?
{
  echo '<p class="text-center">', $message, '</p>'; // Si oui, on l'affiche.
}

if (isset($perso)) // Si on utilise un personnage (nouveau ou pas).
{
?>
  <p class="text-center"><a class="btn btn-secondary" href="?deconnexion=1">Déconnexion</a></p>

  <fieldset class="text-center">
    <legend>Mes informations</legend>
    <p>
      Nom :
      <?= htmlspecialchars($perso->nom()) ?><br />
      Dégâts :
      <?= $perso->degats() ?><br/>
      Niveau :
      <?= $perso->niveau() ?><br/>
      Force :
      <?= $perso->forceperso() ?><br/>
      Limitation :
      <?= $perso->limitation() ?>
    </p>
  </fieldset>

  <fieldset class="text-center">
    <legend>Qui frapper ?</legend>
    <p>
      <?php
$persos = $manager->getList($perso->nom());

if (empty($persos))
{
  echo 'Personne à frapper !';
}

else
{
  foreach ($persos as $unPerso)
  {
    echo '<a href="?frapper=', $unPerso->id(), '">', htmlspecialchars($unPerso->nom()), '</a> (dégâts : ', $unPerso->degats(), ')<br />';
  }
}
?>
    </p>
  </fieldset>
  <?php
}
else
{
?>
  <div class="d-flex py-3">
    <form class="mx-auto" action="" method="post">
      <p>
        <input type="text" name="nom" maxlength="50" placeholder="Nom" /><br />
        <div class="d-flex justify-content-between">
          <input class="btn btn-primary my-1" type="submit" value="Créer" name="creer" /> <input class="btn btn-secondary my-1"
            type="submit" value="Choisir" name="utiliser" />
        </div>
      </p>
    </form>
  </div>
  <?php
}
?>
</body>

</html>
<?php
if (isset($perso)) // Si on a créé un personnage, on le stocke dans une variable session afin d'économiser une requête SQL.
{
  $_SESSION['perso'] = $perso;
}
?>
<div class="container">
  <div class="row">
    <div class="col"><img class="d-flex mx-auto" width="100%" src="assets/image/Yuelia.png" /></div>
    <div class="col"><img class="d-flex mx-auto" width="100%" src="assets/image/olivia.png" /></div>
    <div class="col"><img class="d-flex mx-auto" width="100%" src="assets/image/Noelia.png" /></div>
  </div>
</div>

</body>

</html>